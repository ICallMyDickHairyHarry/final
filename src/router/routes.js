
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/home.vue') },
      { path: '/brands', component: () => import('pages/brands.vue') },
      { path: '/favorites', component: () => import('pages/favorites.vue') },
      { path: '/sale', component: () => import('pages/sale.vue') },
      { path: '/login', component: () => import('pages/login.vue') },
      { path: '/registration', component: () => import('pages/registration.vue') },
      { path: '/bags', component: () => import('pages/bags.vue') },
      { path: '/shoppers', component: () => import('pages/shoppers.vue') },
      { path: '/blog', component: () => import('pages/blog.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
